**10.01**
- Morning meeting.
- Explenation how the eBits works. 
- Deciding on commune comunication place.-Discord
- Understand how works Trello
- Start to work with product description tasks.
- Using Google Drive as document share. 

**11.01**
- Morning meeting.
- Working on new product descriptions.
- Learning about the eBits.

**12.01**
- Morning meeting.
- Brainstorming for new project.
- Working on new product descriptions.

**13.01**
- Morning meeting.
- Brainstorming for new project.
- Working on new product descriptions.


**14.01**
- Morning meeting.
- Working on new product descriptions.
- Validating all our ideas and deciding which project to make.-Postbox allerter.
- And make a small research of what components we need.

> 

**17.01**
- Morning meeting.
- Decidind what parts we are going to use for the project.- ESP32 or ESP8266, Ultrasonic sensor, solar panel and battery. 
- Telling and making order for the needed parts from Nikolaj. 


**18.01**
- Morning meeting.
- Working on web-site until the parts get delivered.
- Make research about the parts. 

**19.01**
- Morning meeting.
- Working on web-site until the parts get delivered.
- Make research about the parts. 
- Search new interesting electronic parts for eBits.

**20.01**
- Morning meeting.
- Working on web-site until the parts get delivered.
- Make research about the parts. 
- Search new interesting electronic parts for eBits.
 

**21.01**
- Morning meeting.
- We get the ordered parts for the project. 
- Start working for the project.
> 


**24.01**
- Morning meeting.
- Assembling the parts and testing them if is working good. 
 

**25.01**
- Morning meeting.
- Preparing the code for the module and testing.
 

**26.01**
- Morning meeting.
- Preparing the code for the module and testing.
- Starting to make the case for the module
 

**27.01**
- Morning meeting.
- Adding battery and solar panel to module to make the final test for next day
- Preparing the code for the module and testing.

**28.01**
- Morning meeting.
- The test of the module was bad because the ultrasonic sensor was now good for this project. 
- Finding a new sensor to use.- KY-032 OBSTACLE-DETECT MODULE
> 

**31.01**
- Morning meeting.
- Working on new sensor.


**01.02**
- Morning meeting.
- Having problem with the new sensor and setting up. 


**02.02**
- Morning meeting. - Making more research how is working the sensor. 
- And testing if is working as intended


**03.02**
- Morning meeting.
- Finding out how is working the sensor and making the new code for it.
- Testing the sensor with new code.


**04.02**
- Morning meeting. 
- Making the final test for new sensor and it was working successfully.
> 


**07.02**
- Morning meeting.
- Change the sensor to KY-033 TRACKING SENSOR because KY-032 OBSTACLE-DETECT MODULE is not easy to use for a begginer and not that precise.
- Search about the new KY-033 TRACKING SENSOR.


**08.02**
- Morning meeting.
- Preparing the code for new sensor.
- Making test of new sensor.
- Making the case for the new parts.

**09.02**
- Morning meeting.
- Final tests of the module. 
- Making the documentation about it. 
- Photos and video of it. 


**10.02**
- Morning meeting.
 
**11.02**
- Morning meeting.
- Fixing some mistakes at the Article

**14.02**
- Morning meeting. 
- Understandin of new project. 

**15.02**
- Morning meeting. 
- Reflecting Back at what I did in the Project

**16.02**
- Morning meeting. 
- Fixing some more things at the Video

**17.02**
- Morning meeting. 
- Redoing the Video
- Participating at the 3D Modelling course done by Nikolaj

**18.02**
- Morning meeting. 
- Waiting for the parts for the new project.
- Making research about the new project.
- Making new Product Description and Putting the Product on the site

**21.02**
- Morning meeting. 
- Making the code for the new project and testing it.

**22.02**
- Morning meeting. 
- Helping an Intership Colleague to make more Gifs for Products
- Publishing the Article

**23.02**
- Morning meeting.
- Helping an Intership Colleague to make more Gifs for Products
- Working on the new project and code. 

**24.02**
- Morning meeting. 
- Nikolaj visit us to bring the ARFcat board and show how to assemble them.
- Helping an Intership Colleague to make more Gifs for Products

**25.02**
- Morning meeting. 
- Making search for the Firebase server.
- Helping an Intership Colleague to make more Gifs for Products

**28.02**
- Morning meeting. 
- Understand how to connect the ESP32 to Firebase.
- Helping an Intership Colleague to make more Gifs for Products

**01.03**
- Morning meeting. 
- Work and understand how to send data to firebase server and ESP32.
- Helping an Intership Colleague to make more Gifs for Products

**02.03**
- Morning meeting. 
- Work and understand how to send data to firebase server and ESP32.
- Helping an Intership Colleague to make more Gifs for Products

**03.03**
- Morning meeting. 
- Work and understand how to send data to firebase server and ESP32.
- Helping an Intership Colleague to make more Gifs for Products

**04.03**
- Morning meeting. 
- Work and understand how to send data to firebase server and ESP32.
- Helping an Intership Colleague to make more Gifs for Products

**07.03**
- Morning meeting. 
- Started to send the digital value from the pins to Firebase server.
- Helping an Intership Colleague to make more Gifs for Products

**08.03**
- Morning meeting. 
- Optimize the digital Value code.

**09.03**
- Morning meeting. 
- Start to work on the Analog value of the pins and test.

**10.03**
- Morning meeting. 
- Working on the code to send Analog Values to server.

**11.03**
- Morning meeting. 
- Working on the code to send Analog Values to server.

**14.03**
- Morning meeting. 
- Working on the code to send Analog Values to server.

**15.03**
- Morning meeting. 
- The analog values of the pins started to be send successfully.
- Start to work on configuration of the pins if to show digital or analog value in server.

**16.03**
- Morning meeting. 
- Working on config part of the code to change the configuration of the pins from an app or server.

**17.03**
- Morning meeting. 
- successfully finished working on configuration part of the code everyting is working good. 
- Optimizing the code.

**18.03**
- Morning meeting. 
- Helping Bogdan for the phone app to make the connection to my server then control and see everyting from phone app.

**21.03**
- Morning meeting. 
- Helping Bogdan for the phone app to make the connection to my server then control and see everyting from phone app.

**22.03**
- Morning meeting. 
- Helping Bogdan for the phone app to make the connection to my server then control and see everyting from phone app.  

**23.03**
- Morning meeting. 
- Helping Bogdan for the phone app to make the connection to my server then control and see everyting from phone app.

**24.03**
- Morning meeting.
- Helping Bogdan for the phone app to make the connection to my server then control and see everyting from phone app.

**25.03**
- Morning meeting.
- Helping Bogdan for the phone app to make the connection to my server then control and see everyting from phone app. 
- Working more on the code. 

**28.03**
- Morning meeting.
- Helping Bogdan for the phone app to make the connection to my server then control and see everyting from phone app. 
- Working more on the code.

**29.03**
- Morning meeting.
- Helping Bogdan for the phone app to make the connection to my server then control and see everyting from phone app. 
- Working more on the code.

**30.03**
- Morning meeting.
- Helping Bogdan for the phone app to make the connection to my server then control and see everyting from phone app.
- Working more on the code.

**31.03**
- Going to Aarhus to meet with Nikolaj and help him for some staff. 

